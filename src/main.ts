class Player {
	readonly id: number;
	readonly auth: string;
	readonly conn: string;
	readonly name: string;
	team: number = 0;
	isAdmin: boolean = false;

	constructor(
		id: number,
		auth: string,
		conn: string,
		name: string
	) {
		this.id = id;
		this.auth = auth;
		this.conn = conn;
		this.name = name;
	}
}

class Tournament {
	public state: "new" | "playing" | "ended" = "new";
	private roundPlayers : Player[] = [];
	private roundWinners : Player[] = [];
	private currentRoundMatch = 0;

	registerPlayer(p: Player): void {
		if ( this.state != "new" ) throw new Error("Tournament already started");
		if ( this.roundPlayers.indexOf(p) != -1 ) throw new Error("Already registered");
		this.roundPlayers.push(p);
	}

	unregisterPlayer(player: Player): void {
		if ( this.state != "new" ) throw new Error("Tournament already started");
		this.roundPlayers = this.roundPlayers.filter(p => p != player);
	}

	start(): void {
		if ( this.state != "new" ) throw new Error("Tournament already started");
		if ( this.roundPlayers.length < 2 ) throw new Error("Need at least two players to start");
		this.state = "playing";
	}

	getMatchPlayers(): Player[] {
		const index = this.currentRoundMatch * 2;
		return [this.roundPlayers[index], this.roundPlayers[index+1]];
	}

	setMatchResult( winner : Player ): void {
		this.roundWinners.push(winner);
		this.currentRoundMatch++;

		if ( this.currentRoundMatch >= this.getNumberOfMatchesForRound() ) {
			this.roundPlayers = this.roundWinners;
			this.roundWinners = [];
			this.currentRoundMatch = 0;
			if ( this.roundPlayers.length <= 1 ) {
				this.state = "ended";
			}
		}
	}

	getNumberOfMatchesForRound() {
		return (this.roundPlayers.length + 1) >> 1;
	}

	getRoundPlayers() {
		return this.roundPlayers;
	}
}

console.log("Running Server...");

const admins = new Set(["bVtUs3APcp1LP1uM37cQvhePDaJYdrwWs0s4TJlldUQ"]); // Add your own player auth here.
const players = new Map<number, Player>();
let tournament = new Tournament();
const room = window.WLInit({
	token: (window as any).WLTOKEN,
	roomName: "Tournament Room",
	maxPlayers: 30,
	public: false,
});
(window as any).WLROOM = room;

room.setSettings( {
	gameMode: "lms",
	teamsLocked: true,
	scoreLimit: 5,
	damageMultiplier: 1,
});

const settings = {
	warmup: 60,
}

const chatCommands : { [index:string] : (player:Player, args: string[]) => void} = {
	register: function(player : Player, args: string[]) {
		tournament.registerPlayer(player);
		room.sendAnnouncement(`${player.name} has registered.`);
	},

	start: function(player: Player, args: string[]) {
		if ( !player.isAdmin ) throw new Error("You need admin rights to start the tournament");
		tournament.start();
	},

	list: function(player: Player, args: string[]) {
		const playerList = tournament.getRoundPlayers().map(p => p.name).join(", ");
		room.sendAnnouncement(playerList, player.id);
	},

	new: function(player: Player, args: string[]) {
		if ( !player.isAdmin ) throw new Error("You need admin rights to create a new tournament");
		t = tournamentLogic();
	},

	help: function(player: Player, args: string[]) {
		let commands = Object.keys(chatCommands).map(c => "!"+c).join(", ");
		room.sendAnnouncement(`Available commands: ${commands}`, player.id);
	},

	warmup: function(player: Player, args: string[]) {
		if (!player.isAdmin) throw new Error("You need admin rights to change the warmup setting");
        if ( args.length < 1 ) throw new Error("warmup <dutation>, expected an argument");
        const duration = parseInt(args[0]);
        if ( isNaN(duration) ) throw new Error("argument should be a duration in seconds");
        settings.warmup = duration;
		room.sendAnnouncement(`Warmup changed to ${duration} seconds by ${player.name}`);
	},
}

function moveAllPlayersToSpectator() {
	for ( let p of room.getPlayerList() ) {
		room.setPlayerTeam(p.id, 0);
	}
}

function *waitForSeconds(seconds: number) {
	const end = window.performance.now() + seconds * 1000;
	while (window.performance.now() < end) {
		yield null;
	}
}

let matchDoneFlag = false;
function *waitForMatchEnd(match : Player[]) {
	matchDoneFlag = false;
	while( true ) {
		let activePlayers = room.getPlayerList().filter(p=>p.team == 1);
		if ( matchDoneFlag || activePlayers.length != 2 ) return;
		yield null;
	}
}

function *tournamentLogic() {
	tournament = new Tournament();
	room.sendAnnouncement("New tournament created!");

	let nextAnnouncementTime = performance.now() + 1000;
	while( tournament.state == "new" ) {
		if ( performance.now() > nextAnnouncementTime ) {
			room.sendAnnouncement("Type !register in chat to join the tournament");
			nextAnnouncementTime = performance.now() + 30000;
		}
		yield null;
	}

	room.sendAnnouncement("The tournament is starting!", undefined, 0xFFFFFF, "bold");
	let numberOfPlayersInRound = tournament.getRoundPlayers().length;
	room.sendAnnouncement(`There are ${numberOfPlayersInRound} players registered.`, undefined);
	
	while( tournament.state == "playing" ) {
		moveAllPlayersToSpectator();
		yield* waitForSeconds(2);

		const currentNumberOfPlayersInRound = tournament.getRoundPlayers().length;
		if ( numberOfPlayersInRound != currentNumberOfPlayersInRound ) {
			// If the number of players changed then a new round has started
			numberOfPlayersInRound = currentNumberOfPlayersInRound;
			if ( numberOfPlayersInRound == 2 ) {
				room.sendAnnouncement(`We have reached the finals!`, undefined, 0xFFFFFF, "bold");
			}else {
				room.sendAnnouncement(`The round of ${numberOfPlayersInRound} is starting`, undefined, 0xFFFFFF, "bold");
			}
			yield* waitForSeconds(1);
		}
		
		let match = tournament.getMatchPlayers();
		if ( match[1] == null ) {
			// The tournament didn't have exactly a power of 2 amount of players...
			room.sendAnnouncement(`${match[0].name} advances to the next round because there is an uneven number of players.`);
			tournament.setMatchResult(match[0]);
		}else {
			room.sendAnnouncement(`Next match: ${match.map(p=>p.name).join(" vs ")}`);
			yield* waitForSeconds(2);
			for ( let p of match ) {
				room.setPlayerTeam(p.id, 1);
			}
			yield* waitForSeconds(2);
			room.restartGame();
			yield* waitForMatchEnd(match);
			let scores = match.map(p => room.getPlayerScore(p.id)?.score ?? 0)
			let winner = scores[0] > scores[1]? match[0] : match[1];
			tournament.setMatchResult(winner);
			room.sendAnnouncement(`${winner.name} wins!`);
		}
		
		yield* waitForSeconds(5);
	}

	let winner = tournament.getRoundPlayers()[0];
	if ( winner != null ) {
		room.sendAnnouncement(`${winner.name} wins the tournament! Congratulations!`, undefined, 0xFFFFFF, "bold");
	}
}

room.onRoomLink = (link) => console.log(link);
room.onCaptcha = () => console.log("Invalid token");
room.onPlayerJoin = (player) => {
	let p = new Player(player.id, player.auth, player.conn, player.name);
	players.set(p.id, p);
	if ( admins.has(p.auth) ) {
		room.setPlayerAdmin(p.id, true);
	}

	room.sendAnnouncement("Welcome to the tournament room!\nType !help to see the available commands.", p.id);
}
room.onPlayerLeave = (p) => {
	var player = players.get(p.id);
	if ( player != null ) {
		try {
			tournament.unregisterPlayer(player);
		}catch(_) {
		}
		player.team = 0;
		players.delete(player.id);
	}
}
room.onPlayerChat = (p, message) => {
	let player = players.get(p.id);
	if ( player == undefined ) return false;

	if ( message[0] == "!" ) {
		// If it start's with '!' then it's a room command
		const commandText = message.substr(1).split(" ");
		console.log(`Command: ${commandText.join(" ")}`);
		const command = chatCommands[commandText[0]];
		if ( command == null ) {
			room.sendAnnouncement(`Unrecognized command: ${commandText[0]}`, player.id);
		}else {
			try {
				command(player, commandText.splice(1));
			} catch(e) {
				if ( e instanceof Error ) {
					room.sendAnnouncement(`Error: ${e.message}`, player.id);
				}else {
					room.sendAnnouncement(`Unknown Error!`, player.id);
				}
			}
		}
		return false;
	}
	return true;
}
room.onPlayerTeamChange = (p, byPlayer) => {
	const player = players.get(p.id);
	if ( player != null ) {
		player.team = p.team;
	}
}
room.onPlayerAdminChange = (p, byPlayer) => {
	const player = players.get(p.id);
	if ( player != null ) {
		player.isAdmin = p.admin;
	}
}
room.onGameEnd = () => {
	matchDoneFlag = true;
}

let t = tournamentLogic();
setInterval(() => t.next(), 100);